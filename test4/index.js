//incluimos la referencia para trabajar como servidor web
const http=require("http");
const hostname = '127.0.0.1';
const port=3000;

//instanciamos servidor

const server = http.createServer((req,res)=>{
    res.statusCode=200;
    res.setHeader('Content-Type','text/html');
    var salida = "<html><head></head><body><h1>Bienvenido al servidor de NodeJs <br> con nodejs</h1>";
    salida+="<h2>Por Gabriel Garcia C</h2>";
    salida+="<img src='https://gabrelgarcia.000webhostapp.com/assets/imagenes/nodelogo.jpeg'>";
    salida+="<p>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'</p>";
    salida+="<a href='https://github.com/Gabbogarco/diplomado'>gabbogarco en github</a><br>";
    salida+="<a href='https://gitlab.com/gabbogarco/dipflutter.git'>gabbogarco en gitlab</a>";
    salida+="</body></html>";
    res.end(salida);
});

server.listen(port,hostname,()=>{
console.log(`El servidor esta corriendo en el puerto http://${hostname}:${port}/`);

});
