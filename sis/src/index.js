
const express = require('express');
//const exphbs = require('express-handlebars');
const { engine } = require('express-handlebars');
const path = require('path');

//inicializacion
const app = express();

//setting

app.set('port', process.env.PORT || 8050);
app.set('views',path.join(__dirname,'views'));
app.use(require('./routes/indexController'));
app.use(require('./routes/homeController'));
app.use(require('./routes/registroController'));
app.use(require('./routes/logoutController'));


app.use(express.static(path.join(__dirname,'public')));


app.engine('.hbs',engine({
    defaultLayout:'main',
    layoutsDir:path.join(app.get('views'),'layouts'),
    partialsDir:path.join(app.get('views'),'partials'),
    extname:'.hbs',
}));


app.set('view engine','hbs');


//levantamos el servidor
app.listen(app.get('port'),() => {
    console.log('El servidor esta corriendo en el puerto',app.get('port'));
});


