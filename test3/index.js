//incluimos la referencia para trabajar como servidor web
const http=require("http");
const hostname = '127.0.0.1';
const port=3000;

//instanciamos servidor

const server = http.createServer((req,res)=>{
    res.statusCode=200;
    res.setHeader('Content-Type','text/plain');
    var salida = '<h1>mi primer aplicacion web <br> con nodejs</h1>';
    res.end(salida);
});

server.listen(port,hostname,()=>{
console.log(`El servidor esta corriendo en el puerto http://${hostname}:${port}/`);

});