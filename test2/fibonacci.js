const prompt = require("prompt-sync")();
console.clear();
console.log("Esta aplicacion genera la serie fibonacci \n Desarrollado por Gabriel Garcia C");
var a=0,b=1,c=0,limite = 0;
limite = prompt("Escribe el límite de la serie: ");
while(c<=limite) {
    a = b;
    b = c;
    process.stdout.write(c+" ");
    c = a + b;
}